import unittest

from solution import remove_duplicates


class TestSolution(unittest.TestCase):
    def test_1(self):
        self.assertEqual(remove_duplicates("hello world"), "helo wrd")

    def test_2(self):
        self.assertEqual(remove_duplicates(""), "")

    def test_3(self):
        self.assertEqual(remove_duplicates("01234567890123456789"), "0123456789")

    def test_4(self):
        self.assertEqual(remove_duplicates("⚽🏀⚽🏀"), "⚽🏀")

    def test_5(self):
        long_string = "".join(["abc" for i in range(10000)])
        self.assertEqual(remove_duplicates(long_string), "abc")
