### Week 4: Remove Duplicates

Write a Python function called `remove_duplicates(string)` that takes in a string as a parameter, and returns a new string that
contains only the unique characters from the original string, in the order that they first appeared.

#### Constraints:

The function should only use lists, tuples, strings, and basic arithmetic and comparison operators (i.e. no sets, dictionaries, or built-in Python functions for removing duplicates).
You should not use any external library.

#### Examples:
```
remove_duplicates("hello world") # should return "helo wrd"
remove_duplicates("abcdefg") # should return "abcdefg"
remove_duplicates("python") # should return "python"
remove_duplicates("mississippi") # should return "misp"
```

You have to implement function `remove_duplicates(string)` in `solution.py` file.
You may create additional functions.

